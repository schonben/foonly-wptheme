<?php
/**
 * The main template file.
 */

// Set up page data
$data = Timber::get_context();

if (is_singular()) {
    $data['post'] = new TimberPost();
} else {
    $data['posts'] = Timber::get_posts();
    $data['pagination'] = Timber::get_pagination();
}

if (is_single()) {
    $data['page'] = 'single';
    $template = 'single';
} elseif (is_page()) {
    $data['page'] = 'page';
    $template = 'page';
} elseif (is_home()) {
    $data['page'] = 'home';
    $template = 'index';
} elseif (is_category()) {
    $data['archive_title'] = get_cat_name(get_query_var('cat'));
    $data['archive_description'] = term_description();
    $data['page'] = 'category';
    $template = 'category';
} elseif (is_tag()) {
    $data['archive_title'] = get_query_var('tag');
    $data['archive_description'] = term_description();
    $data['page'] = 'tag';
    $template = 'category';
} else {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
    $template = "404";
}

//print_r($data);
//exit();

Timber::render("{$template}.twig",$data);
