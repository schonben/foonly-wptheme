# Foonly WP Theme #

This is a WordPress theme build on Timber. Designed for my personal website, but if you like it, feel free to use it.

### How do I get set up? ###

* Download zip
* Install Timber plug-in
* Install theme in WordPress
* Activate
* Enjoy!
