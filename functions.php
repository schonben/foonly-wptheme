<?php

/*====================================
=            Hook Library            =
====================================*/

add_action( 'after_setup_theme', 'foon_setup' );
add_action( 'wp_enqueue_scripts', 'foon_scripts' );

add_filter('timber_context', 'add_to_context');

/*-----  End of Hook Library  ------*/

// Content Width
if ( ! isset( $content_width ) ) {
    $content_width = 767;
}

function foon_setup() {
    // Theme Supports
    add_theme_support( 'custom-header' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'menus' );
    add_theme_support( 'title-tag' );

    show_admin_bar( false );

    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    // Translation Support
    load_theme_textdomain( 'foonly', get_stylesheet_directory() . '/languages' );
    $locale = get_locale();
    $locale_file = get_template_directory() . "/languages/$locale.php";
    if ( is_readable( $locale_file ) )
        require_once( $locale_file );

    // Image Sizes
    // add_image_size( 'nn_list', 730, 288, true );

}
function foon_scripts() {
    wp_enqueue_style("foonly", get_stylesheet_uri());
    //wp_enqueue_script("scrolltofixed",get_template_directory_uri()."/js/scrolltofixed.min.js",array("jquery"),"0.1");
    //wp_enqueue_script("mobile-events",get_template_directory_uri()."/js/jquery.mobile-events.min.js",array("jquery"),"0.1");
    wp_enqueue_script("foonly",get_template_directory_uri()."/js/foonly.js",array("jquery","scrolltofixed"),"0.1");
}

function add_to_context($data){
    /* Now, you add a Timber menu and send it along to the context. */
    $data['menu'] = new TimberMenu(); // This is where you can also send a Wordpress menu slug or ID
    $data['header'] = get_header_image();
    $data['theme_url'] = get_template_directory_uri();
    $data['sidebarleft'] = Timber::get_widgets('sidebar-left');
    $data['sidebarright'] = Timber::get_widgets('sidebar-right');
    $data['sidebarfront'] = Timber::get_widgets('sidebar-front');
    return $data;
}
function foonly_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Left Widget Area', 'foonly' ),
        'id'            => 'sidebar-left',
        'description'   => __( 'Add widgets here to appear on the left side.', 'foonly' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Right Widget Area', 'foonly' ),
        'id'            => 'sidebar-right',
        'description'   => __( 'Add widgets here to appear on the right side.', 'foonly' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Front Page', 'foonly' ),
        'id'            => 'sidebar-front',
        'description'   => __( 'Add widgets here to appear on the front page.', 'foonly' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'foonly_widgets_init' );
